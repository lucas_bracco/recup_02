#include <stdio.h>
#include <string.h>

typedef struct{
    char nombre[20];
    int edad;
}Persona;

int main(){
    Persona x[5],aux;
    int i,j;
    for(i = 0;i < 5;i++){
        printf("Ingrese el nombre de la persona %d:\n",i + 1);
        gets(x[i].nombre);
        printf("Ingrese la edad de la persona %d:\n",i + 1);
        scanf("%d",&x[i].edad);
        fflush(stdin);
    }
    for(i = 0;i < 5;i++){
        for(j = 0;j < 5;j++){
            if(x[i].edad > x[j].edad){
                aux.edad = x[i].edad;
                strcpy(aux.nombre,x[i].nombre);
                x[i].edad = x[j].edad;
                strcpy(x[i].nombre,x[j].nombre);
                x[j].edad = aux.edad;
                strcpy(x[j].nombre,aux.nombre);
            }
        }
    }
    printf("El mayor es %s con %d anios.\n",x[0].nombre,x[0].edad);
    printf("El menor es %s con %d anios.\n",x[4].nombre,x[4].edad);
    return 0;
}